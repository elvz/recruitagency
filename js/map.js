var map = L.map('map').setView([50.438612, 30.521403], 15);

L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

var marker = L.marker([50.43916328683457, 30.523099005221738]).addTo(map);

marker.bindPopup("<b>CyberSearch</b><br>Agency").openPopup();

map.scrollWheelZoom.disable();

map.dragging.disable();


